#!/usr/bin/env bash

###############################################################################
# Copyright 2013-2016, Centrora Security
# URL: http://www.centrora.com
# Email: info@centrora.com
###############################################################################

umask 0177

echo "Installing Centrora Security"
echo "Please enter the absolute path of the Centrora Security Installation, e.g. /home/centrorasuite/public_html";
read centrora_path;

echo "Next, please enter the useraccount, e.g. if your path is /home/centrorasuite/public_html, the account is centrorasuite";
read centrora_user;

echo "Downloading package";
REPONAME="https://github.com/Centrora/centrora-suite/archive/master.zip"
#TODO change the folder name
cd $centrora_path && wget $REPONAME -O 'master.zip' && unzip master.zip
FOLDERNAME=`zipinfo -1 master.zip | head -1 |sed 's/.$//'`
echo "Moving Centrora package";
mv $centrora_path/$FOLDERNAME/* $centrora_path/ && rm -rf $centrora_path/$FOLDERNAME/ && rm master.zip  && chown -R $centrora_user:$centrora_user $centrora_path
if [ ! -e /usr/local/lib/php/ ];
then
mkdir /usr/local/lib/php/;
chmod 0755 /usr/local/lib/php/;
fi;

if [ ! -e /usr/local/lib/php/centrora/ ];
then
mkdir /usr/local/lib/php/centrora;
fi;
cp -R $centrora_path/* /usr/local/lib/php/centrora/

echo "Updating file permissions";
chown -R $centrora_user:$centrora_user /usr/local/lib/php/centrora
find /usr/local/lib/php/centrora -type f -exec chmod 0644 {} \;
find /usr/local/lib/php/centrora -type d -exec chmod 0755 {} \;

if [ ! -e $centrora_path'bk' ];
then
mv $centrora_path $centrora_path'bk'
ln -s /usr/local/lib/php/centrora/ $centrora_path
ln -s /usr/local/lib/php/centrora/ /home/centrora
chown -h $centrora_user:$centrora_user $centrora_path
else
echo "Symlink created already";
fi;

#chnage access rights to enable cron jobs
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected" ];
then
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected";
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp";
echo "access right changed for protected folder";
else
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected/data" ];
then
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp";
echo "access rights changed for data folder ";
else
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath" ];
then
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath";
echo "access rights changed for vsscanpath folder ";
fi;
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp" ];
then
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp";
echo "access rights changed for tmp folder ";
else
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
echo "access rights changed for protected folder ";
fi;
fi;
fi;

if [ ! -d $centrora_path"/media/CentroraBackup" ];
then
    mkdir $centrora_path"/media/CentroraBackup";
    chown -R $centrora_user:$centrora_user $centrora_path"/media/CentroraBackup";
    chmod 0755 $centrora_path"/media/CentroraBackup";
    echo "created CentroraBackup Folder ";
fi;

if [ ! -d $centrora_path"/media/CentroraBackup/Weblog" ];
then
    mkdir $centrora_path"/media/CentroraBackup/Weblog";
    chmod -R 0777 $centrora_path"/media/CentroraBackup/Weblog";
    echo "created Weblog Folder ";
fi;
if [ ! -d $centrora_path"/media/CentroraBackup/WeblogBackup" ];
then
   mkdir $centrora_path"/media/CentroraBackup/WeblogBackup";
    chmod -R 0777 $centrora_path"/media/CentroraBackup/WeblogBackup";
    echo "created WeblogBackup Folder ";
fi;


crontab_filepath="/usr/local/lib/php/centrora/administrator/manifests/whm/centrora.crontab";
echo "Setting up Centrora cron jobs";
cp $crontab_filepath /etc/cron.d/centrora.crontab
if [ -e "/etc/cron.d/centrora.crontab" ]
then
echo "Centrora cron jobs created successfully";
else
echo "failed to create Centrora cron job for, Please contact centrora at support@centrora.com to address this issue";
#end of newly addedd code to support cronjobs
fi;

if [ -e "/home/centrora/administrator/manifests/whm/centrora.conf" ];
then
echo "Ready to move on installing Centrora into WHM";
else
echo "Couldn't find WHM addon file, please re-install the latest version of Centrora Security Suite (version 6.0.8 or above).";
fi;

echo "Creating symbolic link in WHM 3rd Party folder";
if [ ! -e "/usr/local/cpanel/whostmgr/docroot/3rdparty/centrora" ];then
ln -s "/home/centrora" "/usr/local/cpanel/whostmgr/docroot/3rdparty/centrora";
fi;

if [ -e "/usr/local/cpanel/whostmgr/docroot/3rdparty/centrora" ];
then
echo "Successfully created symbolic link in WHM 3rd Party folder, continue...";
else
echo "Failed creating the symbolic link in WHM 3rd Party folder, please contact support team."; exit;
fi;

if [ -e "/usr/local/cpanel/bin/register_appconfig" ]; then
if [ ! -e "/var/cpanel/apps" ]; then
mkdir "/var/cpanel/apps";
chmod 755 "/var/cpanel/apps";
fi;
if [ ! -e "/usr/local/cpanel/3rdparty/php/centrora" ]; then
mkdir "/usr/local/cpanel/3rdparty/php/centrora";
chmod 755 "/usr/local/cpanel/3rdparty/php/centrora";
cp /home/centrora/administrator/php.ini  /usr/local/cpanel/3rdparty/php/centrora/
fi;

/usr/local/cpanel/bin/register_appconfig /home/centrora/administrator/manifests/whm/centrora.conf
if [ -e "/var/cpanel/apps/centrora.conf" ]; then
echo "Successfully creating Centrora Configuration for WHM.";
echo "Restarting cPanel...";
/usr/local/cpanel/etc/init/startcpsrvd;
echo "Congratulations! Centrora Security WHM Plugin Installation has been completed! Please login to WHM and search Centrora Security, you should be able to access the control panel now.";
else
echo "Failed creating Centrora Configuration for WHM, please contact support team."; exit;
fi;
else
echo "Your WHM version is not supported, please upgrade your WHM before continuing..."; exit;
fi;
