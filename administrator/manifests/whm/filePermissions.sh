#!/usr/bin/env bash
umask 0177
echo "Initiating Centrora Installation";
echo "Please enter the absolute path of the Centrora Security Installation, e.g. /home/centrorasuite/public_html";
read centrora_path;

echo "Next, please enter the useraccount, e.g. if your path is /home/centrorasuite/public_html, the account is centrorasuite";
read centrora_user;


#chnage access rights to enable cron jobs
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected" ];
then
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected";
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp";
echo "access right changed for protected folder";
else
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected/data" ];
then
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp";
echo "access rights changed for data folder ";
else
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath" ];
then
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/vsscanPath";
echo "access rights changed for vsscanpath folder ";
fi;
if [ ! -d $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp" ];
then
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
mkdir $centrora_path"/administrator/components/com_ose_firewall/protected/data/tmp";
echo "access rights changed for tmp folder ";
else
chown -R $centrora_user:$centrora_user $centrora_path"/administrator/components/com_ose_firewall/protected";
echo "access rights changed for protected folder ";
fi;
fi;
fi;

if [ ! -d $centrora_path"/media/CentroraBackup" ];
then
    mkdir $centrora_path"/media/CentroraBackup";
    chown -R $centrora_user:$centrora_user $centrora_path"/media/CentroraBackup";
    chmod 0755 $centrora_path"/media/CentroraBackup";
    echo "created CentroraBackup Folder ";
fi;

if [ ! -d $centrora_path"/media/CentroraBackup/Weblog" ];
then
    mkdir $centrora_path"/media/CentroraBackup/Weblog";
    chmod -R 0777 $centrora_path"/media/CentroraBackup/Weblog";
    echo "created Weblog Folder ";
fi;
if [ ! -d $centrora_path"/media/CentroraBackup/WeblogBackup" ];
then
   mkdir $centrora_path"/media/CentroraBackup/WeblogBackup";
   chmod -R 0777 $centrora_path"/media/CentroraBackup/WeblogBackup";
   echo "created WeblogBackup Folder ";
fi;
echo "Changes completed successfully ";

