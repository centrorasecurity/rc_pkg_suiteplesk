function genCombo (fieldLabel, name, mode, option, task, controller, propertyid, keyid, field_subject, field_value, params)
{
	 var combo = new Ext.form.ComboBox({
		            fieldLabel: fieldLabel
		            ,hiddenName: name
		            ,itemId: name
		            ,xtype: 'combo'
				    ,typeAhead: true
				    ,emptyText: 'Please Choose'
				    ,triggerAction: 'all'
				    ,lazyRender:false
				    ,listClass: 'combo-left'
				    ,lastQuery: ''
				    ,mode: mode
				    ,forceSelection: true
				    ,store: new Ext.data.Store({
				  		proxy: new Ext.data.HttpProxy({
				            url: 'index.php?option='+option
				            ,method: 'POST'
			      		})
					  	,baseParams:{task: task, controller: controller + params }
					  	,reader: new Ext.data.JsonReader({
					    	root: 'results'
					    	,totalProperty: 'total'
					    	,idProperty: propertyid
					  	},[
					    {name: 'field_value', type: 'string', mapping: field_value}
					    ,{name: 'subject', type: 'string', mapping: field_subject}
					    ,{name: 'id', type: 'string', mapping: keyid}
					  	])
					})
				    ,valueField: 'field_value'
				    ,displayField: 'subject'
		        });
		    return combo;
}

function sleep(n)
{
     var start = new  Date().getTime();
        while(true)
        if (new Date().getTime()-start> n)
        break;
 }

function oseAjaxForm(option, controller, task, params)
{
	oseMsg('Action in progress','Please wait...');
	params.option = option;
	params.controller = controller;
	params.task = task;
 	Ext.Ajax.request({
				url : 'index.php' ,
				params : params,
				method: 'POST',
				success: function ( result, request ) {
					msg = Ext.decode(result.responseText);
					if (msg.title!='ERROR')
					{
						if (msg.url!=null)
						{
						 	oseMsg(msg.title,msg.content);
						 	oseRedirect(msg.url);
						}
						else
						{
							oseMsg(msg.title,msg.content);
						}
					}
					else
					{
						oseMsg(msg.title,msg.content);
					}
				}
			});
}

function createBox(t, s){
      return '<div class="osemsg"><div>' + t+ '</div><div>' + s + '</div></div>';
}

function oseMsg(title, format){
	  var msgCt;
      if(!msgCt){
	           msgCt = Ext.core.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
	        }
	      var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
	      var m = Ext.core.DomHelper.append(msgCt, createBox(title, s), true);
	      m.slideIn('t').ghost("t", { delay: 1000, remove: true});
	      m.hide();
}

function oseRedirect(url)
{
		window.location = url;
}