<?php
/**
  * @version       1.0 +
  * @package       Open Source Excellence Marketing Software
  * @subpackage    Open Source Excellence Affiates - com_ose_affiliates
  * @author        Open Source Excellence (R) {@link  http://www.opensource-excellence.com}
  * @author        Created on 01-Oct-2011
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2010- Open Source Excellence (R)
*/
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.path');
//require_once( OSEMSC_WAREHOUSE.DS.'public.php' );
class OSEFILEMANController extends JController
{
	protected $controller= null, $_c= null, $com_name = 'com_ose_fileman';
	function __construct()
	{
		parent :: __construct();
	}
	function initControl()
	{
		// Require specific controller if requested
		$this->controller= JRequest :: getWord('controller', null);
		// get the Real Controller
		$this->_c= $this->getController();
	}
	function display($cachable = false, $urlparams = false)
	{
		if(!JRequest :: getWord('view', null))
		{
			JRequest :: setVar('view', 'directories');
		}
		parent :: display();
	}
	function executeTask($task)
	{
		$this->_c->execute($task);
	}
	function getController()
	{
		$controller= $this->controller;
		if($controller)
		{
			require_once(OSEFILEMAN_B_CONTROLLER.DS.$controller.'.php');
			$class= get_class($this).$controller;
			return new $class();
		}
		else
		{
			return $this;
		}
	}
	function redirectE()
	{
		$this->_c->redirect();
	}
	function getMod()
	{
		$result= array();
		$name= JRequest :: getCmd('name', null);
		$type= JRequest :: getCmd('type', null);
		$mod= JRequest :: getCmd('mod', null);
		if (!empty($name) && !empty($type) && !empty($mod))
		{
			echo '<script type="text/javascript">'."\r\n";
			require_once(JPATH_SITE.DS.'administrator'.DS.'components'.DS.$this->com_name.DS.'modules'.DS.$mod.DS.$type.DS.'ext.'.$type.'.'.$name.'.js');
			echo "\r\n".'</script>';
		}
		exit;
	}
	function save($modelName)
	{
		$model= $this->getModel($modelName);
		$update= $model->save();
		if($update)
		{
			$result= array();
			$result['success']= true;
			$result['title']= JText :: _('Done');
			$result['content']= JText :: _('DATA_UPDATE_SUCCESS');
		}
		else
		{
			$result['success']= false;
			$result['title']= JText :: _('Error');
			$result['content']= JText :: _('DATA_UPDATE_FAILED');
		}
		$result= oseJSON :: encode($result);
		oseExit($result);
	}
	function changeStatus($modelName)
	{
		OSESoftHelper :: checkToken();
		$model= $this->getModel($modelName);
		$result= array();
		if($model->changeStatus())
		{
			OSESoftHelper :: returnMessages(true, JText :: _('STATUS_UPDATED_SUCCESS'));
		}
		else
		{
			OSESoftHelper :: returnMessages(false, JText :: _('STATUS_UPDATED_FAILED'));
		}
	}

	function remove($modelName)
	{
		OSESoftHelper :: checkToken();
		$model= $this->getModel($modelName);
		$id = JRequest::getVar('id');
		$result= array();
		if($model->delete($id))
		{
			OSESoftHelper :: returnMessages(true, JText :: _('ITEM_DELETED_SUCCESS'));
		}
		else
		{
			OSESoftHelper :: returnMessages(false, JText :: _('ITEM_DELETED_FAILED'));
		}
	}
	function getOSEItem($modelName, $funcName)
	{
		$model= $this->getModel($modelName);
		$items= $model->$funcName();
		$result= array();
		$result['results']= $items;
		$result['total']= count($items);
		$result= oseJSON :: encode($result);
		oseExit($result);
	}
}
?>