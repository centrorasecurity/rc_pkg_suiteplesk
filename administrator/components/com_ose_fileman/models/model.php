<?php
/**
  * @version       1.0 +
  * @package       Open Source Excellence Marketing Software
  * @subpackage    Open Source Excellence Affiates - com_ose_affiliates
  * @author        Open Source Excellence (R) {@link  http://www.opensource-excellence.com}
  * @author        Created on 01-Oct-2011
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2010- Open Source Excellence (R)
*/
// no direct access
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.model');
class OSEFILEMANModel extends JModelLegacy
{
	function __construct()
	{
		parent :: __construct();
	}
	
	function show_header($title) {
		$url = str_replace( '&dir=', '&ignore=', $_SERVER['REQUEST_URI'] );
		echo "<div align=\"center\">\n";
	}
	
	public function getForm($data = array(), $loadData = true)
	{
	}
	
	function aJaxResponse($success, $msg) 
	{
		if ($success == true)
		{
			$result['success']= true;
			$result['title']= JText :: _('Done');
			$result['status']= JText :: _('Done');

		}
		else
		{
			$result['success']= false;
			$result['title']= JText :: _('ERROR');
			$result['status']= 'ERROR';
		}
		$result['content']= $msg;
		$result= oseJSON :: encode($result);
		oseExit($result);
	}
}
?>