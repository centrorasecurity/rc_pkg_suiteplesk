<?php
/**
 * @version     4.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence File Manager
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 23-Apr-2012
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
// no direct access
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.modeladmin');
class OSEFILEMANModelFiles extends OSEFILEMANModel
{
	var $dir = '';
	var $item = '';
	var $fileName = '';

	function __construct()
	{
		parent :: __construct();
	}

	function del_items($dir, $file) {
		$mainframe=&JFactory::getApplication();
		// delete files/dirs
		if(($GLOBALS["permissions"]&01)!=01)
		{	
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["accessfunc"]);
		}

		$err = false;
		
		if( nx_isFTPMode() ) {
				$abs = get_item_info( $dir,$file);
		} else {
				$abs = get_abs_item($dir,$file);
		}
		$error = null; 
		$file_exists = $GLOBALS['nx_File']->file_exists( $abs ); 	
		if($file_exists==false) {
			$error = $GLOBALS["error_msg"]["itemexist"];
			$err=true;	
		}
		if(!get_show_item($dir, $items[$i])) {
			$error = $GLOBALS["error_msg"]["accessitem"];
			$err=true;	
		}
		if ($err == true)
		{
			$this->aJaxResponse(false, $error); 
		}		
		if( nx_isFTPMode() ) 
		{
			$abs = get_abs_item($dir,$abs);
		}
		// Delete
		$ok= $GLOBALS['nx_File']->remove( $abs );
		
		if($ok===false || PEAR::isError( $ok )) {
			$error=$GLOBALS["error_msg"]["delitem"];
			if( PEAR::isError( $ok ) ) {
				$error.= ' ['.$ok->getMessage().']';
			}
			$this->aJaxResponse(false, $error); 
		}
		else
		{
			return true; 
		}
	}
	function rename_file($dir, $file, $newname)
	{
		if(($GLOBALS["permissions"]&01)!=01) {
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["accessfunc"]);
		}
		
		$newitemname=trim(basename(stripslashes($newname)));
		
		if($newitemname=='' ) {
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["miscnoname"]);
		}
		if( !nx_isFTPMode()) {
			$abs_old = get_abs_item($dir,$file);
			$abs_new = get_abs_item($dir,$newitemname);
		} else {
			$abs_old = get_item_info($dir,$file);
			$abs_new = get_item_info($dir,$newitemname);
		}
		if(@$GLOBALS['nx_File']->file_exists($abs_new)) {
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["itemdoesexist"]);
		}
		
		$perms_old = $GLOBALS['nx_File']->fileperms( $abs_old );
		
		$ok=$GLOBALS['nx_File']->rename( get_abs_item($dir,$file), get_abs_item($dir,$newitemname) );
		
		if( nx_isFTPMode()) {
			$abs_new = get_item_info($dir,$newitemname);
		}
		
		$GLOBALS['nx_File']->chmod( $abs_new, $perms_old );
		
		if($ok===false || PEAR::isError($ok)) {
			$this->aJaxResponse(false, 'Could not rename '.$file.' to '.$newitemname);
		}
		
		$msg = sprintf( $GLOBALS['messages']['success_rename_file'], $item, $newitemname );
		
		$this->aJaxResponse(true, $msg);
		
	}
	
	function extract_item($dir, $file)
	{
		if( !nx_isArchive( $file )) {
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["extract_noarchive"]);
		}
		else {
			$archive_name = realpath(get_abs_item($dir,$file));
			$file_info = pathinfo($archive_name);

			if( empty( $dir )) {
				$extract_dir = realpath($GLOBALS['home_dir']);
			}
			else {
				$extract_dir = realpath( $GLOBALS["home_dir"].DS.$dir );
			}
			$ext = $file_info["extension"];
			require_once(JPATH_COMPONENT.DS."libraries".DS."Archive.php");
			$archive_name .= '/';
			$result = File_Archive::extract( $archive_name, $extract_dir );
			if( PEAR::isError( $result )) {
				$this->aJaxResponse(false, $GLOBALS["error_msg"]["extract_failure"]);
			}
		}
		$this->aJaxResponse(true, $GLOBALS["messages"]["extract_success"]);
	}
	
	function chmod_file($dir, $file, $newperm, $do_recurse)
	{
		if(($GLOBALS["permissions"]&01)!=01) 
		{	
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["accessfunc"]);
		}

		$bin=$newperm;
		
		if( $bin == '0') {
				// Changing permissions to "none" is not allowed
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["permchange"]);
		}
		
		$old_bin = $bin;
		
		if( nx_isFTPMode() ) {
				$mode = decoct(bindec($bin));
				$abs_item = get_item_info( $dir,$file);
		} else {
				$mode = bindec($bin);
				$abs_item = get_abs_item($dir,$file);
		}

		if(!$GLOBALS['nx_File']->file_exists( $abs_item )) {
					$this->aJaxResponse(false, $GLOBALS["error_msg"]["fileexist"]);
		}
			
		if(!get_show_item($dir, $file)) {
					$this->aJaxResponse(false, $GLOBALS["error_msg"]["accessfile"]);
		}

		if( $do_recurse == true) {
					$ok = $GLOBALS['nx_File']->chmodRecursive( $abs_item, $mode );
		}
		else {
			
					if( get_is_dir( $abs_item )) {
						// when we chmod a directory we must care for the permissions
						// to prevent that the directory becomes not readable (when the "execute bits" are removed)
						$bin = substr_replace( $bin, '1', 2, 1 ); // set 1st x bit to 1
						$bin = substr_replace( $bin, '1', 5, 1 );// set  2nd x bit to 1
						$bin = substr_replace( $bin, '1', 8, 1 );// set 3rd x bit to 1
						if( nx_isFTPMode() ) {
							$mode = decoct(bindec($bin));
						} else {
							$mode = bindec($bin);
						}
					}
					$ok = @$GLOBALS['nx_File']->chmod( $abs_item, $mode );
		}
		
		$bin = $old_bin;
			
		if(!$ok || PEAR::isError( $ok ) ) {
			$this->aJaxResponse(false, $abs_item.": ".$GLOBALS["error_msg"]["permchange"]);
		}
		else
		{
			$this->aJaxResponse(true, JText::_('Permission changed successfully for').' '.$abs_item );
		}
	}	
	
	function copy_files($dir, $new_dir, $files, $move = true)
	{
		// ALL OK?
		if(!@$GLOBALS['nx_File']->file_exists(get_abs_dir($new_dir))) 
		{
			$this->aJaxResponse(false, get_abs_dir($new_dir).": ".$GLOBALS["error_msg"]["targetexist"]);
		}
		if(!get_show_item($new_dir,"")) 
		{
			$this->aJaxResponse(false, $new_dir.": ".$GLOBALS["error_msg"]["accesstarget"]);
		}
		if(!down_home(get_abs_dir($new_dir))) 
		{
			$this->aJaxResponse(false, $new_dir.": ".$GLOBALS["error_msg"]["targetabovehome"]);
		}
		
		// copy / move files
		$err=false;

		foreach($files as $file) {
			$tmp = stripslashes($file);
			if( nx_isFTPMode() ) {
				$abs_item = get_item_info($dir,$tmp);
				$abs_new_item = get_item_info('/'.$new_dir,$tmp);
			} else {
				$abs_item = get_abs_item($dir,$tmp);
				$abs_new_item = get_abs_item($new_dir,$tmp);
			}
			if(!@$GLOBALS['nx_File']->file_exists($abs_item)) {
				$this->aJaxResponse(false, $GLOBALS["error_msg"]["itemexist"]);
			}
			if(!get_show_item($dir, $tmp)) {
				$this->aJaxResponse(false, $GLOBALS["error_msg"]["accessitem"]);
			}
			if(@$GLOBALS['nx_File']->file_exists($abs_new_item)) {
				$this->aJaxResponse(false, $GLOBALS["error_msg"]["targetdoesexist"]);
			}

			// Copy / Move
			if($move==false) {
				
				if(@is_link($abs_item) || get_is_file($abs_item)) {
					// check file-exists to avoid error with 0-size files (PHP 4.3.0)
					if( nx_isFTPMode() ) 
					{
						$abs_item = '/'.$dir.'/'.$abs_item['name'];
					}
					$ok=@$GLOBALS['nx_File']->copy( $abs_item ,$abs_new_item); //||@file_exists($abs_new_item);
		
				}
				elseif(@get_is_dir($abs_item)) {
					$dir = nx_isFTPMode() ? '/'.$dir.'/'.$abs_item['name'].'/' : $abs_item;
					if( nx_isFTPMode() ) 
					{
						$abs_new_item .= '/';
					}
					$ok=$GLOBALS['nx_File']->copy_dir( $dir, $abs_new_item);
				}
			}
			else {
				$ok= $GLOBALS['nx_File']->rename($abs_item,$abs_new_item);
			}
			if($ok===false || PEAR::isError( $ok ) ) {
				$error=($action=="copy"?
					$GLOBALS["error_msg"]["copyitem"]:
					$GLOBALS["error_msg"]["moveitem"]
				);
				if( PEAR::isError( $ok ) ) {
					$error.= ' ['.$ok->getMessage().']';
				}
				$this->aJaxResponse(false, $error); 
			}
		}
		if ($ok==true)
		{
			$this->aJaxResponse(true, ($move==false?JText::_('File successfully copied'):JText::_('File successfully moved')));
		}
	}

	function upload_files($dir, $files)
	{
		if(($GLOBALS["permissions"]&01)!=01) 
		{
			show_error($GLOBALS["error_msg"]["accessfunc"]);
		}
		// Execute
		$cnt=count($GLOBALS['__FILES']['userfile']['name']);
		$err=false;
		$err_avaliable=isset($GLOBALS['__FILES']['userfile']['error']);

		// upload files & check for errors
		for($i=0;$i<$cnt;$i++) {
				$errors[$i]=NULL;
				$tmp = $GLOBALS['__FILES']['userfile']['tmp_name'][$i];
				$items[$i] = stripslashes($GLOBALS['__FILES']['userfile']['name'][$i]);
				if($err_avaliable) $up_err = $GLOBALS['__FILES']['userfile']['error'][$i];
				else $up_err=(file_exists($tmp)?0:4);
				$abs = get_abs_item($dir,$items[$i]);
		
				if($items[$i]=="" || $up_err==4) continue;
				if($up_err==1 || $up_err==2) {
					$errors[$i]=$GLOBALS["error_msg"]["miscfilesize"];
					$err=true;	continue;
				}
				if($up_err==3) {
					$errors[$i]=$GLOBALS["error_msg"]["miscfilepart"];
					$err=true;	continue;
				}
				if(!@is_uploaded_file($tmp)) {
					$errors[$i]=$GLOBALS["error_msg"]["uploadfile"];
					$err=true;	continue;
				}
				if(@file_exists($abs) && empty( $_REQUEST['overwrite_files'])) {
					$errors[$i]=$GLOBALS["error_msg"]["itemdoesexist"];
					$err=true;	continue;
				}
					
				// Upload
				$ok = @$GLOBALS['nx_File']->move_uploaded_file($tmp, $abs);
					
				if($ok===false || PEAR::isError( $ok )) {
					$errors[$i]=$GLOBALS["error_msg"]["uploadfile"];
					if( PEAR::isError( $ok ) ) $errors[$i].= ' ['.$ok->getMessage().']';
					$err=true;	continue;
				}
				elseif( !nx_isFTPMode()  ) {
					@$GLOBALS['nx_File']->chmod( $abs, 0644 );
				}
			}
		
			if($err) {
				// there were errors
				$err_msg="";
				for($i=0;$i<$cnt;$i++) {
					if($errors[$i]==NULL) continue;
					$err_msg .= $items[$i]." : ".$errors[$i]."<BR>\n";
				}
				show_error($err_msg);
			}
		header("Location: ".make_link("directories",$dir,NULL));
		return;
	}
	
	function archive_item($dir, $files)
	{
		if(($GLOBALS["permissions"]&01)!=01) 
		{
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["accessfunc"]);
		}
		if(!$GLOBALS["zip"] && !$GLOBALS["tgz"]) 
		{
			$this->aJaxResponse(false,$GLOBALS["error_msg"]["miscnofunc"]);
		}
		
		$allowed_types = array( 'zip', 'tgz', 'tbz', 'tar' );

		// If we have something to archive, let's do it now
		if( !file_exists( get_abs_dir($dir ) )) {
				$this->aJaxResponse(false,'The Save-To Directory you have specified does not exist.');
		}
		if( !is_writable( get_abs_dir($dir))) {
			$this->aJaxResponse(false,'Please specify a writable directory to save the archive to.');
		}
		require_once( OSEFILEMAN_B_PATH.DS.'libraries'.DS.'Archive.php' );

		while( @ob_end_clean() );
		header('Status: 200 OK');

		$files_per_step = 25000;

		$abs_dir=get_abs_dir($dir);
		
		$name=basename(stripslashes($dir.'_'.date('md_hi').'.zip'));
	
		$download = JArrayHelper::getValue( $_REQUEST, 'download', "n" );
		$startfrom = JArrayHelper::getValue( $_REQUEST, 'startfrom', 0 );
		
		$archive_name = get_abs_item($dir,$name);
		
		$fileinfo = pathinfo( $archive_name );
			
		if( empty( $fileinfo['extension'] )) {
				$archive_name .= ".zip";
				$fileinfo['extension'] = 'zip';
		}
		foreach ($files as $file) {
			$selitem=stripslashes($file);
			if( is_dir( $abs_dir ."/". $selitem )) {
				$items = JFolder::files($abs_dir .DS.  $selitem, '.', true, true );
				foreach ( $items as $item ) {
					if( is_dir( $item ) || !is_readable( $item ) || $item == $archive_name ) continue;
						$v_list[] = $item;
				}
			}
			else {
					$v_list[] = $selitem;
				}
			}
			$cnt_filelist = count( $v_list );
			$remove_path = $GLOBALS["home_dir"];
			if( $dir ) {
				$remove_path .= $dir.$GLOBALS['separator'];
			}
			for( $i=$startfrom; $i < $cnt_filelist && $i < ($startfrom + $files_per_step); $i++ ) 
			{
				$filelist[] = File_Archive::read( $v_list[$i], str_replace($remove_path, '', $v_list[$i] ) );
			}
			//echo '<strong>Starting from: '.$startfrom.'</strong><br />';
			//echo '<strong>Files to process: '.$cnt_filelist.'</strong><br />';
			//print_r( $filelist );exit;
		
			// Do some setup stuff
			if (function_exists('ini_set'))
			{
				ini_set('memory_limit', '512M');
			}
			@set_time_limit( 0 );
			error_reporting( E_ERROR | E_PARSE );
			$result = File_Archive::extract( $filelist, $archive_name );
		
			if( PEAR::isError( $result ) ) {
				$this->aJaxResponse(false, $name.": Failed saving Archive File. Error: ".$result->getMessage());
			}
			else
			{
				$this->aJaxResponse(true, $name.": Archive file has been created successfully. ");
			}
		
	}
	
	function make_item($dir, $mkname, $mktype, $symlink_target)
	{
		if(($GLOBALS["permissions"]&01)!=01) 
		{
			$this->aJaxResponse(false,$GLOBALS["error_msg"]["accessfunc"]);
		}
		
		$mkname=basename(stripslashes($mkname));
		
		if($mkname=="") 
		{
			$this->aJaxResponse(false, $GLOBALS["error_msg"]["miscnoname"]);
		}
		
		$new = get_abs_item($dir,$mkname);
		if(@$GLOBALS['nx_File']->file_exists($new)) 
		{
			$this->aJaxResponse(false, $mkname.": ".$GLOBALS["error_msg"]["itemdoesexist"]);
		}
		
		if($mktype=="dir") {
			$ok=$GLOBALS['nx_File']->mkdir($new, 0777);
			$err=$GLOBALS["error_msg"]["createdir"];
		} elseif( $mktype == 'file') {
			$ok=$GLOBALS['nx_File']->mkfile($new);
			$err=$GLOBALS["error_msg"]["createfile"];
		} elseif( $mktype == 'symlink' ) {
			if( empty( $symlink_target )) {
				$this->aJaxResponse(false, 'Please provide a valid <strong>target</strong> for the symbolic link.');
			}
			if( !file_exists($symlink_target) || !is_readable($symlink_target)) {
				$this->aJaxResponse(false, 'The file you wanted to make a symbolic link to does not exist or is not accessible by PHP.');
			}
			$ok = symlink( $symlink_target, $new );
			$err = 'The symbolic link could not be created.';
		}
		
		if($ok===false || PEAR::isError( $ok )) {
			if( PEAR::isError( $ok ) ) $err.= $ok->getMessage();
			$this->aJaxResponse(false, $err);
		}
		else
		{
			$this->aJaxResponse(true, JText::_('Item created successfully'));
		}
	}
}