<?php
/**
  * @version     4.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence File Manager
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 23-Apr-2012
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/

// no direct access
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.modeladmin');
class OSEFILEMANModelDownload extends OSEFILEMANModel
{
	function __construct()
	{
		parent :: __construct();
	}
	function download_item($dir, $item, $unlink=false) {
		// download file
		global $action;
		// Security Fix:
		$item=basename($item);
	
		while( @ob_end_clean() );
		ob_start();
	
		if( nx_isFTPMode() ) {
			$abs_item = $dir.'/'.$item;
		}
		else {
			$abs_item = get_abs_item($dir,$item);
			if( !strstr( $abs_item, realpath($GLOBALS['home_dir']) ))
			$abs_item = realpath($GLOBALS['home_dir']).$abs_item;
		}
	
		if(($GLOBALS["permissions"]&01)!=01) show_error($GLOBALS["error_msg"]["accessfunc"]);
		if(!$GLOBALS['nx_File']->file_exists($abs_item)) show_error($item.": ".$GLOBALS["error_msg"]["fileexist"]);
		if(!get_show_item($dir, $item)) show_error($item.": ".$GLOBALS["error_msg"]["accessfile"]);
	
		if( nx_isFTPMode() ) {
	
			$abs_item = nx_ftp_make_local_copy( $abs_item );
			$unlink = true;
		}
		$browser=id_browser();
		header('Content-Type: '.(($browser=='IE' || $browser=='OPERA')?
			'application/octetstream':'application/octet-stream'));
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.filesize(realpath($abs_item)));
		//header("Content-Encoding: none");
		if($browser=='IE') {
			header('Content-Disposition: attachment; filename="'.$item.'"');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
		} else {
			header('Content-Disposition: attachment; filename="'.$item.'"');
			header('Cache-Control: no-cache, must-revalidate');
			header('Pragma: no-cache');
		}
		@set_time_limit( 0 );
		@readFileChunked($abs_item);
	
		if( $unlink==true ) {
			unlink( $abs_item );
		}
		ob_end_flush();
		nx_exit();
	}
}
?>