<?php
/**
  * @version       1.0 +
  * @package       Open Source Excellence Marketing Software
  * @subpackage    Open Source Excellence Affiates - com_ose_affiliates
  * @author        Open Source Excellence (R) {@link  http://www.opensource-excellence.com}
  * @author        Created on 01-Oct-2011
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2010- Open Source Excellence (R)
*/
defined('_JEXEC') or die(';)');
class OSEFILEMANControllerFiles extends OSEFILEMANController
{
	function __construct()
	{
		parent :: __construct();
	}
	function del_item()
	{
		OSESoftHelper::oseadmin_check(); 
		$session = JFactory::getSession();
		
		$dir = $session ->get('dir');
		$files = $session ->get('files');
		$files = OSEJSOn::decode($files, true);
		
		if (empty ($files) && empty ($dir))
		{
			$dir = JRequest::getVar('dir'); 
			$files = JRequest::getVar('files');
		}
		
		if (!is_array($files))
		{
			$files = array($files); 
		}
		
		$model = $this->getModel('files');
		foreach ($files as $file)
		{
			$result = $model -> del_items($dir, $file);
		} 
		if ($result == true)
		{
			$session ->clear('dir');
			$session ->clear('files');
			$model->aJaxResponse(true, $GLOBALS['messages']['success_delete_file']) ; 
		}
		else
		{
			$session ->clear('dir');
			$session ->clear('files');
		}
	}
	function archive_item()
	{
		OSESoftHelper::oseadmin_check();
		$session = JFactory::getSession();
		
		$dir = $session ->get('dir');
		$files = $session ->get('files');
		$files = OSEJSOn::decode($files, true);
		
		if (empty ($files) && empty ($dir))
		{
			$dir = JRequest::getVar('dir');
			$files = JRequest::getVar('files');
		}
		
		if (!is_array($files))
		{
			$files = array($files);
		}
		
		$model = $this->getModel('files');

		$result = $model -> archive_item($dir, $files);
		if ($result == true)
		{
			$session ->clear('dir');
			$session ->clear('files');
			$model->aJaxResponse(true, $GLOBALS['messages']['success_delete_file']) ;
		}
		else
		{
			$session ->clear('dir');
			$session ->clear('files');
		}
	}
	function extract_item()
	{
		OSESoftHelper::oseadmin_check();
		$dir = JRequest::getVar('dir');
		$file = JRequest::getVar('files');
		$model = $this->getModel('files');
		$model -> extract_item($dir, $file);
	}
	function make_item()
	{
		OSESoftHelper::oseadmin_check();
		$dir = JRequest::getVar('dir');
		$mkname = JRequest::getVar('mkname');
		$mktype = JRequest::getVar('mktype');
		$symlink_target = JRequest::getVar('symlink_target');
		$model = $this->getModel('files');
		$model -> make_item($dir, $mkname, $mktype, $symlink_target);
	}
	function rename_file()
	{
		OSESoftHelper::oseadmin_check();
		$dir = JRequest::getVar('dir');
		$file = JRequest::getVar('file');
		$newname = JRequest::getVar('newname');
		$model = $this->getModel('files');
		$model -> rename_file($dir, $file, $newname);
	}
	function chmod_file()
	{
		OSESoftHelper::oseadmin_check();
		$dir = JRequest::getVar('dir');
		$file = JRequest::getVar('file');
		$newperm = JRequest::getVar('newperm');
		$do_recurse = JRequest::getVar('do_recurse');
		$model = $this->getModel('files');
		$model -> chmod_file($dir, $file, $newperm, $do_recurse);
	}
	function add_initdir()
	{
		OSESoftHelper::oseadmin_check();
		$session = JFactory::getSession();
		$scanInfo = $session->get("scanInfo");
		
		if (empty($scanInfo))
		{
			$scanInfo= array();
			$scanInfo['selected'] = array(); 
		}
		$dir = JRequest::getVar('checkdir'); 
		if (!empty($dir))
		{
			$scanInfo['selected'][]= $dir ; 
			$scanInfo['selected'] = $this->cleanFileArray($scanInfo['selected']); 
			$session->set("scanInfo",$scanInfo);
			
			$model = $this->getModel('files');
			$model->aJaxResponse(true, JText::_('Files have been added into the database initialization list, would you like to start the Database initialization?'));
		}
		else
		{
			$model = $this->getModel('files');
			$model->aJaxResponse(false, JText::_('Directory cannot be empty'));
		}
	}
	function addtosession()
	{
		OSESoftHelper::oseadmin_check();
		$session = JFactory::getSession(); 
		$session ->set('dir', JRequest::getVar('dir')); 
		$session ->set('files', JRequest::getVar('files'));
		$model = $this->getModel('files');
		$model->aJaxResponse(true, 'Success');
	}
	function addtoAVsession()
	{
		OSESoftHelper::oseadmin_check();
		$session = JSession::getInstance('oseantivirus',array());
		$scanInfo = $session->get("scanInfo");
		if (empty($scanInfo))
		{
			$scanInfo= array();
			$scanInfo['selected'] = array(); 
		}
		$dir = JRequest::getVar('dir'); 
		$files = JRequest::getVar('files');
		$files = oseJSON::decode($files); 
		if (!empty($files))
		{
			foreach ($files as $file)
			{
				$abs = get_abs_item($dir,$file);
				$scanInfo['selected'][]= $abs ; 
			}
		}
		$scanInfo['selected'] = $this->cleanFileArray($scanInfo['selected']); 
		$session->set("scanInfo",$scanInfo );
		$model = $this->getModel('files');
		$model->aJaxResponse(true, JText::_('Files have been added into the database initialization list, would you like to start the Database initialization?'));
	}
	function clearAVsession()
	{
		OSESoftHelper::oseadmin_check();
		$session = JSession::getInstance('oseantivirus',array());
		$session->clear("scanInfo");
		
		$model = $this->getModel('files');
		$model->aJaxResponse(true, JText::_('Session cleared.'));
	}
	
	function cleanFileArray($vars)
	{	
		if (empty($vars))
		{
			return $vars;
		}
		$i = 0; 
		foreach ($vars as $var)
		{
			$file_exists = $GLOBALS['nx_File']->file_exists( $var );
			if ($file_exists ==false)
			{
				unset($vars[$i]); 
			}
		}
		$vars= array_unique($vars); 
		return $vars; 
				
	}
	function copy_files()
	{
		OSESoftHelper::oseadmin_check();
		$session = JFactory::getSession();
		$dir = $session ->get('dir');
		$files = $session ->get('files');
		$files = OSEJSOn::decode($files); 
		$newDir = JRequest::getVar('newDir');
		$newDir = str_replace ($GLOBALS["home_dir"], '', $newDir); 
		$model = $this->getModel('files');
		$model -> copy_files($dir, $newDir, $files, false);
	}
	function move_files()
	{
		OSESoftHelper::oseadmin_check();
		$session = JFactory::getSession();
		$dir = $session ->get('dir');
		$files = $session ->get('files');
		$files = OSEJSOn::decode($files);
		$newDir = JRequest::getVar('newDir');
		$newDir = str_replace ($GLOBALS["home_dir"], '', $newDir);
		$model = $this->getModel('files');
		$model -> copy_files($dir, $newDir, $files, true);
	}
	
	function upload_files()
	{
		OSESoftHelper::oseadmin_check();
		$dir = JRequest::getVar('dir');
		$files = JRequest::getVar('files');
		$file = JRequest::getVar('file');
		$model = $this->getModel('files');
		$model -> upload_files($dir, $files);
	}
}
?>