<?php
/**
  * @version     4.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence File Manager
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 23-Apr-2012
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(";)");
// Define some properties
define('OSEFILEMAN', 'com_ose_fileman');
define('OSEFILEMANVERSION', $OSESoftHelper->getVersion());
define('OSEFILEMANTITLE','OSE File Manager™');

define('OSEFILEMANFOLDER', 'components/'.OSEFILEMAN);
define('OSEFILEMAN_B_PATH', JPATH_ADMINISTRATOR.DS.'components'.DS.OSEFILEMAN);
define('OSEFILEMAN_B_PATH_FTP', JPATH_ADMINISTRATOR.DS.'components'.DS.OSEFILEMAN.DS.'ftp_tmp');
define('OSEFILEMAN_B_CONTROLLER', OSEFILEMAN_B_PATH.DS.'controllers');
define('OSEFILEMAN_B_MODEL', OSEFILEMAN_B_PATH.DS.'models');
define('OSEFILEMAN_B_VIEW', OSEFILEMAN_B_PATH.DS.'views');
define('OSEFILEMAN_B_MODULES', OSEFILEMAN_B_PATH.DS.'modules');
define('OSEFILEMAN_B_HELPERS', OSEFILEMAN_B_PATH.DS.'helpers');
define('OSEFILEMAN_B_URL', JURI :: root().'/administrator/'.OSEFILEMANFOLDER);
define('OSEFILEMAN_F_PATH', JPATH_SITE.DS.'components'.DS.OSEFILEMAN);
define('OSEFILEMAN_F_CONTROLLER', OSEFILEMAN_F_PATH.DS.'controllers');
define('OSEFILEMAN_F_MODEL', OSEFILEMAN_F_PATH.DS.'models');
define('OSEFILEMAN_F_VIEW', OSEFILEMAN_F_PATH.DS.'views');

//------------------------------------------------------------------------------
if(defined('E_STRICT')) {
	// Suppress Strict Standards Warnings (E_ALL doesn't include E_STRICT!)
	error_reporting(E_ALL);
}
$version = new JVersion();
$version = substr($version->getShortVersion(),0,3);
if(!defined('JOOMLA16'))
{
	$value = ($version >= '1.6')?true:false;
	define('JOOMLA16',$value);
}
if(!defined('JOOMLA30'))
{
	$value = ($version >= '3.0' && $version <='5.0')?true:false;
	define('JOOMLA30',$value);
}

?>