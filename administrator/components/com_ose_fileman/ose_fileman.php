<?php
/**
  * @version     4.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence File Manager
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 23-Apr-2012
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(";)");
if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
// INSTALLATION;
$installedPhpVersion= floatval(phpversion());
$supportedPhpVersion= 5.2;
$install= JRequest :: getVar('install', '', 'REQUEST');
$view= JRequest :: getVar('view', '', 'GET');
$task= JRequest :: getVar('task', '', 'REQUEST');
$component= 'com_ose_fileman';
//install
if(((file_exists(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.$component.DS.'installer.dummy.ini') || $install)) ||($installedPhpVersion < $supportedPhpVersion))
{
	$app = JFactory::getApplication();
	$app ->JComponentTitle = 'OSE Installer';
	// Require the define file
	require_once(dirname(__FILE__).DS.'helpers'.DS.'osesofthelper.php');
	$OSESoftHelper= new OSESoftHelper();
	require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.$component.DS.'define.php');
	require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.$component.DS.'installer.helper.php');
	$oseInstaller= new oseInstallerHelper();
	$oseInstaller->install();
	$document = JFactory::getDocument();
	$document->addScript(JURI::root().'media/system/js/mootools-core.js');
}
else
{
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_cpu'.DS.'define.php');
require_once(OSECPU_B_PATH.DS.'oseregistry'.DS.'oseregistry.php');
oseRegistry :: register('registry', 'oseregistry');
oseRegistry :: call('registry');
		
// Require the define file
require_once(dirname(__FILE__).DS.'helpers'.DS.'osesofthelper.php');
$OSESoftHelper = new OSESoftHelper ();
require_once(JPATH_COMPONENT.DS.'define.php');
require_once(dirname(__FILE__).DS.'helpers'.DS.'osefmhelper.php');
require_once(JPATH_COMPONENT.DS.'init.php');
// Require the base controller
if (JOOMLA30==true)
{	
	require_once(OSEFILEMAN_B_CONTROLLER.DS.'controller.php');
	require_once(OSEFILEMAN_B_MODEL.DS.'model.php');
	require_once(OSEFILEMAN_B_VIEW.DS.'view.php');
}
else
{
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'legacy'.DS.'controller.php');
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'legacy'.DS.'model.php');
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'legacy'.DS.'view.php');	
}	
// OSE Files

// Create the Controller
$_c= new OSEFILEMANController();
$_c->initControl();
// Perform the Request task
$_c->executeTask(JRequest :: getCmd('task', null));
// Redirect if set by the controller
$_c->redirectE();
}