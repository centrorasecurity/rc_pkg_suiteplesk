<?php
/**
 * @version     4.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence File Manager
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 23-Apr-2012
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */

defined('_JEXEC') or die("Direct Access Not Allowed");

?>
<div id="oseheader">
	<div class="container">
		<div class="logo-labels">
			<h1>
				<a href="http://www.opensource-excellence.com" target="_blank"><?php echo JText::_("Open Source Excellence"); ?>
				</a>
			</h1>
				<?php
					echo $this->preview_menus; 
				?>
		</div>
				<?php
					oseSoftHelper::showmenu();
				?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?></div>
		<div class="grid-title"><?php echo JText::_('Please edit your files here').'.'.JText::_("Your current location is").": <div id ='fileName' >".$this->fileName.'</div>';  ?></div>
		<div id ='ose-fileman-edit'>

		<table class="adminform">
			<tr>
			  <td>
				<div id="editorarea">
					<textarea class="<?php echo $this->fileType; ?>" style="width:95%;" name="codearea" id="codearea" rows="25" cols="120" wrap="off" >
						<?php echo $this->fileContent; ?>
					</textarea>
				</div>
				<br/>
			  </td>
			</tr>
			<tr>
			  <td>
				<div class='button_bar'>
					<input type='hidden' name='savefileName' id = 'savefileName' value = '<?php echo $this->fileName; ?>'/>
					<input type='hidden' name='returnDir' id = 'returnDir' value = '<?php echo dirname($this->fileName); ?>'/>
					
					<button id='savebtn' name ='savebtn'><?php echo $GLOBALS["messages"]["btnsave"]; ?></button>
					<button id='closebtn' name ='closebtn'><?php echo $GLOBALS["messages"]["btnclose"]; ?></button>					
				</div>
			  </td>
			</tr>
		</table>

		<?php echo OSESoftHelper::getToken();  ?>
		</div>
	</div>
 </div>
</div>

<script id="script">
window.onload = function() {
	var te = document.getElementById("codearea");
	//var sc = document.getElementById("script");
	te.value = (te.textContent || te.innerText || te.innerHTML).replace(/^\s*/, "");
	te.innerHTML = "";

	var foldFunc = CodeMirror.newFoldFunction(CodeMirror.braceRangeFinder);
	window.editor = CodeMirror.fromTextArea(te, {
		mode: "<?php echo $this->fileType; ?>",
		lineNumbers: true,
        matchBrackets: true,		
		lineWrapping: true,
        indentUnit: 4,
        indentWithTabs: true,
        enterMode: "keep",
        tabMode: "shift",
		onGutterClick: foldFunc,
		extraKeys: {
			"Ctrl-Q": function(cm){
				foldFunc(cm, cm.getCursor().line);
			}
		}
	});
};
</script>

<?php
echo oseSoftHelper::renderOSETM();
?>