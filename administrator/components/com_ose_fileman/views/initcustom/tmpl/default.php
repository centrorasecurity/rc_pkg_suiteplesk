<?php
/**
 * @version     4.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence File Manager
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 23-Apr-2012
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */

defined('_JEXEC') or die("Direct Access Not Allowed");
?>

<div id="oseheader">
	<div class="container">
		<div class="logo-labels">
			<h1>
				<a href="http://www.opensource-excellence.com" target="_blank"><?php echo JText::_("Open Source Excellence"); ?>
				</a>
			</h1>
				<?php
					echo $this->preview_menus; 
				?>
		</div>
				<?php
					oseSoftHelper::showmenu();
				?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?></div>
		<div class="grid-title"><?php echo JText::_('Please enter the directory you would like to add to the database initialization process here.');  
			 echo '<br/>'.JText::_('Current directories in database initialization list.').'<br/>'; 
			 $session = JSession::getInstance('oseantivirus',array());
			 $scanInfo = $session->get("scanInfo");
			 if (!empty($scanInfo))
			 {
			    foreach ($scanInfo['selected'] as $s)
				{
						echo $s.'<br />';
				}
			 }
		?></div>
		<div id ='ose-fileman-edit'>
		

	<table class="adminform">
		<tr>
			<td colspan="2">
				
			</td>
		</tr>
	 	<tr>
	 		<td colspan="2"> 
	 			<label for="dir"><?php echo $GLOBALS["messages"]["newname"]; ?> </label>&nbsp;
	 			<input name="dir" id="dir" type="text" size="60" value="" />
	 		</td>
	 	</tr>
		<tr class='button_bar'>
			<td colspan="2" style='text-align: right'>
				<button id ="changebtn"><?php echo $GLOBALS["messages"]["btnadd"]; ?></button>
		    	<button id ="closebtn"><?php echo $GLOBALS["messages"]["btncancel"]; ?></button>
				<input type='hidden' name='returnDir' id = 'returnDir' value = '<?php echo dirname(get_abs_item($this->dir, $this->fileName)); ?>'/>
		    </td>
		</tr>
	</table>
	
	<?php echo OSESoftHelper::getToken();  ?>
		</div>
	</div>
 </div>
</div>


<?php
echo oseSoftHelper::renderOSETM();
?>