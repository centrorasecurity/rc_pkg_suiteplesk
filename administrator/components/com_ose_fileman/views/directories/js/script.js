Ext.ns('ose.Fileman');

	function Toggle(e) {
		if(e.checked) {
			Highlight(e);
			document.selform.toggleAllC.checked = AllChecked();
		} else {
			UnHighlight(e);
			document.selform.toggleAllC.checked = false;
		}
   	}

	function ToggleAll(e) {
		if(e.checked) CheckAll();
		else ClearAll();
	}

	function CheckAll() {
		var ml = document.selform;
		var len = ml.elements.length;
		for(var i=0; i<len; ++i) {
			var e = ml.elements[i];
			if(e.name == "selitems[]") {
				e.checked = true;
				Highlight(e);
			}
		}
		ml.toggleAllC.checked = true;
	}

	function ClearAll() {
		var ml = document.selform;
		var len = ml.elements.length;
		for (var i=0; i<len; ++i) {
			var e = ml.elements[i];
			if(e.name == "selitems[]") {
				e.checked = false;
				UnHighlight(e);
			}
		}
		ml.toggleAllC.checked = false;
	}

	function Copy() {
		if(NumChecked()==0) {
			alert("Please select at least one item");
			return;
		}
		selected=new Object();
		
		ml = document.selform;
		len = ml.elements.length;
		
		for(var i=0; i<len; ++i) {
			if(ml.elements[i].name == "selitems[]" && ml.elements[i].checked) 
				{
					selected[i] = ml.elements[i].value; 
				}
		}
		params=new Object();
		params.controller = 'files';
		params.files = Ext.encode(selected);
		params.task = 'addtosession';
		params.dir = Ext.get('dir').getValue(); 
		
		Ext.Ajax.request({
			url: 'index.php?option=com_ose_fileman',
        	params: params,
			success: function(response, opts){
				window.location = '../../../../../../index.php'
			}
		});
	}
	function DbinitCustom() {
		window.location = '../../../../../../index.php'
	}
	function Dbinit() {
		if(NumChecked()==0) {
			alert("Please select at least one item");
			return;
		}
		var selected=new Object();
		ml = document.selform;
		len = ml.elements.length;
		
		for(var i=0; i<len; ++i) {
			if(ml.elements[i].name == "selitems[]" && ml.elements[i].checked) 
				{
					selected[i] = ml.elements[i].value; 
				}
		}
		params=new Object();
		params.controller = 'files';
		params.files = Ext.encode(selected);
		params.task = 'addtoAVsession';
		params.dir = Ext.get('dir').getValue(); 
		
		Ext.Ajax.request({
			url: 'index.php?option=com_ose_fileman',
        	params: params,
			success: function(result, opts){
				msg = Ext.decode(result.responseText);
				Ext.Msg.confirm(msg.title, msg.content, function(btn,txt)	{
					if(btn == 'yes')	{
               			window.location = '../../../../../../index.php'
               		}
				}); 	
				
			}
		});
	}
	function DbinitClear()
	{
		params=new Object();
		params.controller = 'files';
		params.task = 'clearAVsession';
		
		Ext.Ajax.request({
			url: 'index.php?option=com_ose_fileman',
        	params: params,
			success: function(result, opts){
				msg = Ext.decode(result.responseText);
				ose.Fileman.msg.setAlert(msg.status, msg.content);
			}
		});
	}
	
	function Move() {
		if(NumChecked()==0) {
			alert("Please select at least one item");
			return;
		}
		selected=new Object();
		
		ml = document.selform;
		len = ml.elements.length;
		
		for(var i=0; i<len; ++i) {
			if(ml.elements[i].name == "selitems[]" && ml.elements[i].checked) 
				{
					selected[i] = ml.elements[i].value; 
				}
		}
		params=new Object();
		params.controller = 'files';
		params.files = Ext.encode(selected);
		params.task = 'addtosession';
		params.dir = Ext.get('dir').getValue(); 
		
		Ext.Ajax.request({
			url: 'index.php?option=com_ose_fileman',
        	params: params,
			success: function(response, opts){
				window.location = '../../../../../../index.php'
			}
		});
	}
	function Delete() {
		if(NumChecked()==0) {
			alert("Please select at least one item");
			return;
		}
		selected=new Object();
		
		ml = document.selform;
		len = ml.elements.length;
		
		for(var i=0; i<len; ++i) {
			if(ml.elements[i].name == "selitems[]" && ml.elements[i].checked) 
				{
					selected[i] = ml.elements[i].value; 
				}
		}
		params=new Object();
		params.controller = 'files';
		params.files = Ext.encode(selected);
		params.task = 'addtosession';
		params.dir = Ext.get('dir').getValue(); 
		
		Ext.Ajax.request({
			url: 'index.php?option=com_ose_fileman',
        	params: params,
			success: function(response, opts){
				var dirSelected = document.getElementById('dir');  
				var fileSelected = document.getElementById('item_0'); 
				ajaxAction('com_ose_fileman', 'files', 'del_item', dirSelected.value, fileSelected.value); 
			}
		});
	}
	function Archive() {
		if(NumChecked()==0) {
			alert("Please select at least one item");
			return;
		}
		selected=new Object();
		
		ml = document.selform;
		len = ml.elements.length;
		
		for(var i=0; i<len; ++i) {
			if(ml.elements[i].name == "selitems[]" && ml.elements[i].checked) 
				{
					selected[i] = ml.elements[i].value; 
				}
		}
		params=new Object();
		params.controller = 'files';
		params.files = Ext.encode(selected);
		params.task = 'addtosession';
		params.dir = Ext.get('dir').getValue(); 
		
		Ext.Ajax.request({
			url: 'index.php?option=com_ose_fileman',
        	params: params,
			success: function(response, opts){
				var dirSelected = document.getElementById('dir');  
				var fileSelected = document.getElementById('item_0'); 
				ajaxAction('com_ose_fileman', 'files', 'archive_item', dirSelected.value, fileSelected.value); 
			}
		});
	}
	function AllChecked() {
		ml = document.selform;
		len = ml.elements.length;
		for(var i=0; i<len; ++i) {
			if(ml.elements[i].name == "selitems[]" && !ml.elements[i].checked) return false;
		}
		return true;
	}

	function NumChecked() {
		ml = document.selform;
		len = ml.elements.length;
		num = 0;
		for(var i=0; i<len; ++i) {
			if(ml.elements[i].name == "selitems[]" && ml.elements[i].checked) ++num;
		}
		return num;
	}


	// Row highlight

	function Highlight(e) {
		var r = null;
		if(e.parentNode && e.parentNode.parentNode) {
			r = e.parentNode.parentNode;
		} else if(e.parentElement && e.parentElement.parentElement) {
			r = e.parentElement.parentElement;
		}
		if(r && r.className=="rowdata") {
			r.className = "rowdatasel";
		}
	}

	function UnHighlight(e) {
		var r = null;
		if(e.parentNode && e.parentNode.parentNode) {
			r = e.parentNode.parentNode;
		} else if (e.parentElement && e.parentElement.parentElement) {
			r = e.parentElement.parentElement;
		}
		if(r && r.className=="rowdatasel") {
			r.className = "rowdata";
		}
	}
	
	function ajaxAction(option, controller, task, dir, file) {
		//Ext.MessageBox.wait('Please wait while the action is performed', 'Performing Actions');
		var i = 0;
		Ext.Ajax.request({
			url : 'index.php',
			params : {
				option : option,
				task : task,
				controller : controller,
				dir: dir,
				files: file
			},
			method : 'POST',
			success : function(result, request) {
				msg = Ext.decode(result.responseText);
				if (msg.status != 'ERROR') {
					Ext.MessageBox.hide();
					Ext.Msg.alert(msg.title, msg.content, function(btn,txt)	{
						if(btn == 'ok')	{
	               			window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+dir+'&order=name&srt=yes'
	               		}
					}); 	
					
					
				} else {
					Ext.MessageBox.hide();
					ose.Fileman.msg.setAlert('Error', msg.content);
					
				}
			}
		});
	}
	
	function createfile()
	{
		var mkname = Ext.get('mkname').dom.value; 
		var mktype = Ext.get('mktype').dom.value;
		var symlink_target = Ext.get('symlink_target').dom.value;
		var dirSelected = document.getElementById('dir').value;  
		Ext.Ajax.request({
			url : 'index.php',
			params : {
				option : 'com_ose_fileman',
				controller : 'files',
				task : 'make_item',
				dir: dirSelected,
				mkname: mkname,
				mktype: mktype,
				symlink_target: symlink_target
			},
			method : 'POST',
			success : function(result, request) {
				msg = Ext.decode(result.responseText);
				if (msg.status != 'ERROR') {
					Ext.MessageBox.hide();
					Ext.Msg.alert(msg.title, msg.content, function(btn,txt)	{
						if(btn == 'ok')	{
	               			window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+dirSelected+'&order=name&srt=yes'
	               		}
					}); 	
					
					
				} else {
					Ext.MessageBox.hide();
					ose.Fileman.msg.setAlert('Error', msg.content);
					
				}
			}
		});
	}
	
	Ext.onReady(function(){
		ose.Fileman.msg = new Ext.App();
	});