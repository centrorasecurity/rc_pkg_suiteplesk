<?php
/**
 * @version     2.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Centrora Security Firewall
 * @subpackage    Open Source Excellence WordPress Firewall
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 01-Jun-2013
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2012- ... Open Source Excellence
 */
// Basic configuration;
if (!defined('ODS'))
{
	define('ODS', DIRECTORY_SEPARATOR);
}
define('OFRONTENDSCAN', true);
define('OSE_SUITE', true);
define('OSEFWDIR', dirname(__FILE__).ODS.'components/com_ose_firewall/');
require_once(dirname(__DIR__) . ODS.'configuration.php');
require_once(OSEFWDIR.'assets'.ODS.'config'.ODS.'define.php');
require_once(OSE_FWFRAMEWORK.ODS.'oseFirewallJoomla.php');
//require_once(OSEFWDIR.'/vendor/autoload.php');
$ready = oseFirewall::preRequisitiesCheck();
if ($ready == false)
{
	if (oseFirewall::isBackendStatic())
	{
		oseFirewall::showNotReady();
	}
	else
	{
		return;
	}
}
// Load the OSE Framework ;
$oseFirewall = new oseFirewall();
$oseFirewall->initSystem();
$ready = oseFirewall::isDBReady();
if ($ready == true)
{
    ////FIREWALL SCANNER V7
    $oseFirewall::callLibClass('fwscannerv7', 'fwscannerv7');
    $fwscannerv7 = new oseFirewallScannerV7();
    $settings = $fwscannerv7->getFirewallSettingsfromDb();
    if ($settings['status'] == 1 && $settings['info'][1] == 1) {
        //scan the traffic using the fwscanner v7
        $fwscannerv7->scanTraffic($settings['info'], 'GET');
        $fwscannerv7->scanTraffic($settings['info'], 'POST');

    } else {
        $firewallv6_settings = $oseFirewall::getConfiguration('scan');
        if ($firewallv6_settings['success'] == 1 && isset($firewallv6_settings['data']['devMode'])) {
            if( $firewallv6_settings['data']['devMode'] == 0)
            {
                $oseFirewall->enhanceSysSecurity();
                $isAdvanceFirewallScanner = $oseFirewall->isAdvanceFirewallSettingEnable();
                if ($isAdvanceFirewallScanner == true) {
                    oseFirewall::callLibClass('fwscanner', 'fwscannerad');
                    $oseFirewallScanner = new oseFirewallScannerAdvance();
                    $oseFirewallScanner->hackScan();
                } else {
                    oseFirewall::callLibClass('fwscanner', 'fwscannerbs');
                    $oseFirewallScanner = new oseFirewallScannerBasic();
                    $oseFirewallScanner->hackScan();
                }
            }
        }
    }




}
?>