<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	Templates.bluestork
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

$app = JFactory::getApplication();
$doc = JFactory::getDocument();

$doc->addStyleSheet('templates/system/css/system.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');

if ($this->direction == 'rtl') {
	$doc->addStyleSheet('templates/'.$this->template.'/css/template_rtl.css');
}

/** Load specific language related css */
$lang = JFactory::getLanguage();
$file = 'language/'.$lang->getTag().'/'.$lang->getTag().'.css';
if (JFile::exists($file)) {
	$doc->addStyleSheet($file);
}

if ($this->params->get('textBig')) {
	$doc->addStyleSheet('templates/'.$this->template.'/css/textbig.css');
}

if ($this->params->get('highContrast')) {
	$doc->addStyleSheet('templates/'.$this->template.'/css/highcontrast.css');
}
require_once(dirname(__FILE__).'/osePanel.php');
$osePanel = new osePanel();
$oseVersion = $osePanel->getVersion();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo  $this->language; ?>" lang="<?php echo  $this->language; ?>" dir="<?php echo  $this->direction; ?>" >
	<head>
		<jdoc:include type="head" />

		<!--[if IE 7]>
			<link href="templates/<?php echo  $this->template ?>/css/ie7.css" rel="stylesheet" type="text/css" />
		<![endif]-->
	</head>
<body id="minwidth-body">
	<div id="border-top" class="h_blue">
		<div class="oselogo"></div>
        <div class="ose-version"><?php echo 'OSE Security 2015 '; ?></div>
	</div>
	<div id="header-box">
		<div id="module-status">
			<jdoc:include type="modules" name="status"/>
			<?php
				//Display an harcoded logout
				$task = JRequest::getCmd('task');
				if ($task == 'edit' || $task == 'editA' || JRequest::getInt('hidemainmenu')) {
					$logoutLink = '';
				} else {
					$logoutLink = JRoute::_('index.php?option=com_login&task=logout&'. JSession::getFormToken() .'=1');
				}
				$hideLinks	= JRequest::getBool('hidemainmenu');
				$output = array();
				// Print the Preview link to Main site.
				$output[] = '<span class="viewsite"><a href="'.JURI::root().'" target="_blank">'.JText::_('JGLOBAL_VIEW_SITE').'</a></span>';
				// Print the logout link.
				$output[] = '<span class="logout">' .($hideLinks ? '' : '<a href="'.$logoutLink.'">').JText::_('JLOGOUT').($hideLinks ? '' : '</a>').'</span>';
				// Output the items.
				foreach ($output as $item) :
				echo $item;
				endforeach;
			?>
		</div>
		<div id="module-menu">
			<jdoc:include type="modules" name="menu"/>
		</div>
		<div class="clr"></div>
	</div>
	<div id="content-box">
		<div id="element-box">
			<jdoc:include type="message" />
			<div class="m" >
				<div class="adminform">
					<div class="cpanel-left">
						<?php if ($this->countModules('icon')>1):?>
							<?php echo JHtml::_('sliders.start', 'position-icon', array('useCookie' => 1));?>
							<jdoc:include type="modules" name="icon" style="sliders" />
							<?php echo JHtml::_('sliders.end');?>
						<?php else:?>
							<jdoc:include type="modules" name="icon" />
						<?php endif;?>
					</div>
					<div class="cpanel-right">
						<jdoc:include type="component" />
					</div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
		<noscript>
			<?php echo  JText::_('JGLOBAL_WARNJAVASCRIPT') ?>
		</noscript>
	</div>
	<jdoc:include type="modules" name="footer" style="none"  />
	<div id="footer">
	<p class="oselogo">
		<a href="http://www.opensource-excellence.com" target="_blank">
			<img src ="templates/bluestork/images/logo.png" height="50px" weight="50px" />
		</a>	
	</p>
	<p class="copyright">
		Centrora Security is a portfolio of Open Source Excellence. &copy; 2008 - <?php echo date("Y"); ?> <a href="http://www.opensource-excellence.com" target="_blank">Open Source Excellence &#0174;</a>. All Rights Reserved. <br /> Credits to: <a href="http://www.joomla.org" target="_blank">Joomla!&#0174;</a> A Free Software released under the GNU/GPL License.
	</p>
	</div>
</body>
</html>
