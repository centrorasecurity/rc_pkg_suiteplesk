<?php 
class osePanel {
	public $timezone = 'America/Los_Angeles';
	public $oemClass = null;
	public function __construct () {
		$this->loadOEMClass () ;
	}
	public function getVersion() {
		$xml = JFactory::getXML(JPATH_ADMINISTRATOR .'/components/com_ose_firewall/ose_firewall.xml');
		$version = (string)$xml->version;
		return 'Version: '.$version; 
	}
	private function getJEDVersion () {
		if (!function_exists('allow_url_fopen') && !function_exists('ini_set'))
		{
			return 'N/A';
		}
		else if (!function_exists('allow_url_fopen') && function_exists('ini_set'))
		{
			ini_set('allow_url_fopen', 'on');
		}
		if ($this->isVersionChecked () == false)
		{
			$tmp = $this->getJEDVersionFromWeb ();
			$v = $this->getVersionChecked (); 
			if (empty($v))
			{
				$this->insertVersionChecked ($tmp[0]);
			}
			return $tmp[0];
		}
		else 
		{
			$version = $this->getVersionChecked ();
			return $version; 
		}
	}
	private function getJEDVersionFromWeb () {
		return  OSESUITEVERSION; 
	}
	private function getTimeZone () {
		if (function_exists('ini_get'))
		{
			$timezone = ini_get('date.timezone');
			if (empty($timezone))
			{
				$this->setTimeZone ();  
			}
			else
			{
				$timezone = date_default_timezone_get();
				$this->setTimeZone ($timezone); 
			}
		}
	}
	private function setTimeZone ($timezone=null) {
		if (function_exists('ini_set') && empty($timezone))
		{
			ini_set('date.timezone', $this->timezone);
		}	
		else
		{
			$this->timezone = $timezone;
		}
	}
	private function isVersionChecked () {
		$db = JFactory::getDBO(); 
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'version_checkedsut' ";
		$db->setQuery($query); 
		$date = $db->loadResult();
		if (empty($date)) {
			return false;
		}
		else
		{
			$this->getTimeZone ();
			$tz_object = new DateTimeZone($this->timezone);
			$datetime1 = new DateTime($date, $tz_object);
			$datetime2 = new DateTime("now", $tz_object);
			$interval = round(($datetime2->format('U') - $datetime1->format('U')) / (60*60*24)); 
			if ($interval >=1)
			{
				return false;
			}
			else 
			{
				return true; 
			}
		}
	}
	private function getVersionChecked () {
		$db = JFactory::getDBO(); 
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'version_osesut' ";
		$db->setQuery($query);
		return $db->loadResult();
	}
	private function insertVersionChecked ($version) {
		$db = JFactory::getDBO(); 
		$query = "INSERT INTO `#__ose_secConfig` 
				  (`id`, `key`, `value`, `type`, `default`) VALUES
				  (NULL, 'version_checkedsut', '".date("Y-m-d")."', 'global', '');";
		$db->setQuery($query); 
		$db->query();

		$query = "INSERT INTO `#__ose_secConfig` 
				  (`id`, `key`, `value`, `type`, `default`) VALUES
				  (NULL, 'version_osesut', ". $db->Quote($version).", 'global', '');";
		$db->setQuery($query); 
		$db->query();
	}
	public function getMenus () {
		
		return oseFirewall::getmenus();
	}
	public function loadOEMClass () {
		if (!defined('OSE_WORDPRESS_FIREWALL')) {
			$oem_id = $this->getOEMCustomerID();
			if (!empty($oem_id) && file_exists(JPATH_ADMINISTRATOR.'/components/com_ose_firewall/classes/Library/oem/'.$oem_id.'.php')) {
				require_once(JPATH_ADMINISTRATOR.'/components/com_ose_firewall/classes/Library/oem/'.$oem_id.'.php');
				$oemClassName= 'CentroraOEM'.$oem_id;
				$this->oemClass = new $oemClassName($oem_id);
				$this->oemClass->defineVendorName();
			}
			else {
				define('OSE_WORDPRESS_FIREWALL', 'Centrora Security™');
				if (!defined('JOOMLA15')) {
					define('JOOMLA15', false);
				}
	            define('OSE_WORDPRESS_FIREWALL_SHORT', 'Centrora');
	            define('OSE_OEM_URL_MAIN', 'https://www.centrora.com/');
	            define('OSE_OEM_URL_HELPDESK', 'https://www.centrora.com/support/');
	            define('OSE_OEM_URL_MALWARE_REMOVAL', 'https://www.centrora.com/malware-removal/');
	            define('OSE_OEM_URL_ADVFW_TUT', 'https://docs.centrora.com/en/latest/config-firewall-settings.html');
	            define('OSE_OEM_URL_PREMIUM_TUT', 'https://www.centrora.com/store/activating-premium-service');
	            define('OSE_OEM_URL_AFFILIATE', 'http://www.centrora.com/affiliate-partners/');
	            define('OSE_OEM_URL_SUBSCRIBE', 'http://www.centrora.com/store/centrora-subscriptions');
	            define('OSE_OEM_LANG_TAG','');
			}
		}
	}
	private function getOEMCustomerID () {
		$customer_id ='';
		if (file_exists(dirname(__DIR__).'/oem.data')) {
			$customer_id = file_get_contents(dirname(__DIR__).'/oem.data'); 
		}
		if (!empty($customer_id)) {
			return $customer_id;
		}
		else {
			$db = JFactory::getDBO();
			$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'customer_id' AND `type` = 'oem'";
			$db->setQuery($query);
			return $db->loadResult();
		}
	}
	public function getLogoURL () {
		if (!empty ($this->oemClass) ) {
			return $this->oemClass->getLogoURL();
		}
		else {
			return 'components/com_ose_firewall/public/images/logo5.png';
		}
	} 
	public function getTopBarURL () {
		if (!empty ($this->oemClass) ) {
			return $this->oemClass->getTopBarURL();
		}
		else {
			return '<li><a title="Affiliate" href="//www.centrora.com/store/index.php?route=affiliate/login"><i class="fa fa-magnet"></i> <span class="hidden-xs hidden-sm hidden-md">Affiliate</span> </a></li>
			<li><a title="My Account" href="https://www.centrora.com/store/index.php?route=account/login"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">My Account</span> </a></li>
			<li><a title="Support" id="support-center" href="https://www.centrora.com/support-center/"><i class="im-support"></i> <span class="hidden-xs hidden-sm hidden-md">Support</span></a></li>
			<li><a title="Subscription" href="http://www.centrora.com/"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md">Subscription</span></a></li>
			<li><a title="Tutorial" href="http://www.centrora.com/tutorial/"><i class="im-stack-list"></i> <span class="hidden-xs hidden-sm hidden-md">Tutorial</span></a></li>
			<li><a title="Malware Removal" href="http://www.centrora.com/cleaning"><i class="im-spinner10"></i> <span class="hidden-xs hidden-sm hidden-md">Malware Removal</span></a></li><li><a title="Home" href="index.php">Quick links:&nbsp;&nbsp;&nbsp;<i class="im-home7"></i> <span class="hidden-xs hidden-sm hidden-md">Centrora</span> </a></li>';
		}
	}
	public function showFooter () {
	if (!empty ($this->oemClass) ) {
			return $this->oemClass->showFooter();
		}
		else {
			return '<div class="footer-bottom" style="background: none;">
		    <div class="container" style="color:white; background: none;">
		      <p class="pull-left">
		        Centrora Security is a portfolio of Open Source Excellence. &copy; 2008 - <?php echo date("Y"); ?> <a
					href="http://www.opensource-excellence.com" target="_blank" style="color:white;"><b>Open
					Source Excellence &#0174;</b>b></a>. All Rights Reserved. <br /> Credits
				to: <a href="http://www.joomla.org" target="_blank"  style="color:white;"><b>Joomla!&#0174;</b></a>
				A Free Software released under the GNU/GPL License.
		      </p>
		      <div class="pull-right paymentMethodImg">
				<a href="http://www.centrora.com" target="_blank">
		        	<img src="https://abf15001ec03fa4a6d04d66b0863e67be2b16af7.googledrive.com/host/0B4Hl9YHknTZ4R3pMTHRyMlZuTEk/centrora_logo_footer.png" height="50px" weight="50px" style="margin-right: 10px;" />
		        </a>
		        <a href="http://www.opensource-excellence.com" target="_blank">
		        	<img src="templates/bluestork/images/logo.png" height="50px" weight="50px" />
		        </a>
		      </div>
		    </div>
		  </div>';
		}
	}
}
?>