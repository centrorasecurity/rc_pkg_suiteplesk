<?php
/**
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
defined ( '_JEXEC' ) or die ();
require_once(dirname(__FILE__) . '/osePanel.php');
global $osePanel;
$osePanel = new osePanel ();
$doc = JFactory::getDocument ();
// Include the component HTML helpers.
JHtml::addIncludePath ( JPATH_COMPONENT . '/helpers/html' );
// Add Stylesheets
$doc->addStyleSheet ( '../media/system/css/system.css' );
$doc->addStyleSheet ( 'template/css/template.css' );
if ($this->direction == 'rtl') {
	$doc->addStyleSheet ( 'template/css/template_rtl.css' );
}
$doc->addStyleSheet ( '../administrator/components/com_ose_firewall/public/css/jquery.dataTables.min.css' );
$doc->addStyleSheet ( '../administrator/components/com_ose_firewall/public/css/bootstrap.css' );
$doc->addStyleSheet ( '../administrator/components/com_ose_firewall/public/css/waitme.less.css' );
$doc->addStyleSheet ( '../administrator/components/com_ose_firewall/public/css/icons.css' );
$doc->addStyleSheet ( '../administrator/components/com_ose_firewall/public/css/main.css' );
$doc->addStyleSheet ( '../administrator/components/com_ose_firewall/public/css/v4.css' );
// Load the JavaScript behaviors
JHtml::_ ( 'behavior.framework', true );
JHtml::_ ( 'behavior.keepalive' );
JHtml::_ ( 'behavior.tooltip' );
JHtml::_ ( 'behavior.formvalidation' );
JHtml::_ ( 'script', 'installation/template/js/installation.js', true, false, false, false );

if (!empty($osePanel->oemClass)) {
	$doc->addStyleSheet (OSE_FWPUBLICURL. '/css/oem/'.$osePanel->oemClass->customer_id.'/custom.css' );
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?php echo $this->language; ?>"
	lang="<?php echo $this->language; ?>"
	dir="<?php echo $this->direction; ?>">
<head>
<jdoc:include type="head" />

<!--[if IE 7]>
			<link href="template/css/ie7.css" rel="stylesheet" type="text/css" />
		<![endif]-->
<script type="text/javascript">
			window.addEvent('domready', function() {
				window.Install = new Installation('rightpad', '<?php echo JURI::current(); ?>');

				Locale.define('<?php echo JFactory::getLanguage()->getTag(); ?>', 'installation', {
					sampleDataLoaded: '<?php echo JText::_('INSTL_SITE_SAMPLE_LOADED', true); ?>'
				});
				Locale.use('<?php echo JFactory::getLanguage()->getTag(); ?>');
			});
 		</script>
</head>
<body class="contentpane">
	<div class="container">
		<nav role="navigation" class="navbar navbar-default">
				    <div class="everythingOnOneLine">
						<div class="col-lg-12">
							<?php 
								if (!empty($osePanel->oemClass)) {
									echo $osePanel->oemClass->addLogo (); 
								}
								else {
							?>
							<div class="logo">
								<img width="250px" alt="Centrora Logo" src="<?php echo OSE_FWPUBLICURL.'images/topbar/whitelogo.png'; ?>">
							</div>
							<?php 
								}
							?>
							<div id="versions"> <div class="version-updated"> <?php echo $osePanel->getVersion();?></div></div>
						</div>
					</div>
					<div class="navbar-top">
						 <div class="col-lg-1 col-sm-6 col-xs-6 col-md-6">
								<div class="pull-left">
								</div>
						 </div>
						 <div class="col-lg-11 col-sm-6 col-xs-6 col-md-6">
							 <div class="pull-right">
										<ul class="userMenu ">
										<?php 
											echo $osePanel->getTopBarURL();
										?>
										</ul>
							 </div>
						</div>
					</div>
		</nav>
		<jdoc:include type="message" />
		<div id="warning">
			<noscript>
				<div id="javascript-warning">
						<?php echo JText::_('INSTL_WARNJAVASCRIPT'); ?>
				</div>
			</noscript>
		</div>
		<div id="content-inner">
			<div class="row ">
				<div class="col-lg-12 sortable-layout">
					<div id="jst_0" class="panel panel-primary plain toggle panelClose panelRefresh">
<!--						<div class="panel-heading white-bg">-->
<!--						  <h4 class="panel-title"></h4>-->
<!--						     <div class="panel-controls">-->
<!--						     </div>-->
<!--						</div>-->
						<div class="panel-body">
							<div class="col-lg-3 col-md-3 sortable-layout">
								<?php echo JHtml::_('installation.stepbar'); ?>
								<div class="box"></div>
							</div>

							<div class="col-lg-9 col-md-9 sortable-layout">
								<div id="rightpad">
									<jdoc:include type="installation" />
								</div>
							</div>
							<div class="clr"></div>
						</div>	
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<footer>
  		  <?php echo $osePanel->showFooter(); ?>
	  </footer>
		</div>
	</div>
	</div>
</body>
</html>
