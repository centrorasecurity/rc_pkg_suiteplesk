<?php
/**
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="panel panel-default">
    <div class="panel-heading white-bg">
		<h3 class="panel-title"><?php echo JText::_('INSTL_COMPLETE'); ?></h3>
    </div>
    <div class="panel-controls-buttons">
<?php if ($this->document->direction == 'ltr') : ?>
		<button class="btn btn-success btn-sm mr5 mb10"  onClick ="location.href='<?php echo JURI::root(); ?>';" ><?php echo JText::_('JSITE'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10"  onClick ="location.href='<?php echo JURI::root(); ?>administrator/';"><?php echo JText::_('JADMINISTRATOR'); ?></button>
<?php elseif ($this->document->direction == 'rtl') : ?>
		<button class="btn btn-success btn-sm mr5 mb10"  onClick ="location.href='<?php echo JURI::root(); ?>';" ><?php echo JText::_('JSITE'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10"  onClick ="location.href='<?php echo JURI::root(); ?>administrator/';"><?php echo JText::_('JADMINISTRATOR'); ?></button>
<?php endif; ?>
	</div>
</div>

<form action="index.php" method="post" id="adminForm" class="form-validate">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo JText::_('INSTL_COMPLETE_TITLE'); ?></h4>
        </div>
		<div class="m">
			<div class="install-text">
				<p><?php echo JText::_('INSTL_COMPLETE_DESC1'); ?></p>
				<p><?php echo JText::_('INSTL_COMPLETE_DESC2'); ?></p>
				<p><?php echo JText::_('INSTL_COMPLETE_DESC3'); ?></p>
			</div>
			<div class="install-body">
				<div class="m">
					<fieldset>
						<table class="final-table">
							<tr>
								<td class="error">
									<?php echo JText::_('INSTL_COMPLETE_REMOVE_INSTALLATION'); ?>
								</td>
							</tr>
							<tr>
								<td><input class="button" type="button" name="instDefault" value="<?php echo JText::_('INSTL_COMPLETE_REMOVE_FOLDER'); ?>" onclick="Install.removeFolder(this);"/></td>
							</tr>
							<tr class="message inlineError" id="theDefaultError" style="display: none">
								<td>
									<dl>
										<dt class="error"><?php echo JText::_('JERROR'); ?></dt>
										<dd id="theDefaultErrorMessage"></dd>
									</dl>
								</td>
							<tr>
							<tr>
								<td>
									<h3>
									<?php echo JText::_('INSTL_COMPLETE_ADMINISTRATION_LOGIN_DETAILS'); ?>
									</h3>
								</td>
							</tr>
							<tr>
								<td class="notice">
									<?php echo JText::_('JUSERNAME'); ?> : <strong><?php echo $this->options['admin_user']; ?></strong>
								</td>
							</tr>
							<tr>
								<td>&#160;</td>
							</tr>
							<?php if ($this->config) : ?>
							<tr>
								<td class="small">
									<?php echo JText::_('INSTL_CONFPROBLEM'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<textarea rows="5" cols="49" name="configcode" onclick="this.form.configcode.focus();this.form.configcode.select();" ><?php echo $this->config; ?></textarea>
								</td>
							</tr>
							<?php endif; ?>
						</table>
					</fieldset>
				</div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
