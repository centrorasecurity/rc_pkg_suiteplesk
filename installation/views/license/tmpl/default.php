<?php
/**
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="panel panel-default">
    <div class="panel-heading white-bg">
		<h3 class="panel-title"><?php echo JText::_('INSTL_LICENSE'); ?></h3>
    </div>
    <div class="panel-controls-buttons">
<?php if ($this->document->direction == 'ltr') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('preinstall');" rel="prev" title="<?php echo JText::_('JPREVIOUS'); ?>"><?php echo JText::_('JPREVIOUS'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('database');" rel="next" title="<?php echo JText::_('JNEXT'); ?>"><?php echo JText::_('JNEXT'); ?></button>
<?php elseif ($this->document->direction == 'rtl') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('database');" rel="next" title="<?php echo JText::_('JNEXT'); ?>"><?php echo JText::_('JNEXT'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('preinstall');" rel="prev" title="<?php echo JText::_('JPREVIOUS'); ?>"><?php echo JText::_('JPREVIOUS'); ?></button>
<?php endif; ?>
	</div>
</div>
<form action="index.php" method="post" id="adminForm" class="form-validate">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo JText::_('INSTL_GNU_GPL_LICENSE'); ?></h4>
        </div>
		<div class="m" style="padding-left: 0px; padding-right: 0px; margin-top: 5px;">
			<iframe src="gpl.html" class="license" marginwidth="25" scrolling="auto"></iframe>
		</div>
	</div>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
