<?php
/**
 * @version     2.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Centrora Security Firewall
 * @subpackage    Open Source Excellence WordPress Firewall
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 01-Jun-2013
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2012- ... Open Source Excellence
 */
defined('_JEXEC') or die("Direct Access Not Allowed");
if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}
jimport('joomla.plugin.plugin');
class plgSystemOsefirewall extends JPlugin
{
	public function plgSystemOsefirewall(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	public function onAfterInitialise()
	{
		$mainframe = JFactory::getApplication('SITE');
		if ($mainframe->isAdmin())
		{
			return; // Dont run in admin
			}
		else
		{
			if (!defined('ODS'))
			{
				define('ODS', DIRECTORY_SEPARATOR);
			}
			define('OFRONTENDSCAN', false);
			define('OSEFWDIR', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_firewall'.DS);
			require_once(OSEFWDIR.ODS.'protected'.ODS.'config'.ODS.'define.php');
			require_once(OSE_FWFRAMEWORK.ODS.'oseFirewallJoomla.php');
			require_once (OSEFWDIR.ODS.'protected'.ODS.'library'.ODS.'RemoteLogin'.ODS.'RemoteLogin.php');
			// Load the OSE Framework ;
			$oseFirewall = new oseFirewall();
			oseFirewall::loadRequest();	
			$oseFirewall->initSystem();
			oseFirewall::runYiiApp();
			$ready = oseFirewall::isDBReady();
			if ($ready == true)
			{
				$signatureUpdate = JRequest::getInt('signatureUpdate', 0);
				if ($signatureUpdate == true)
				{
					$this->signatureUpdate ();
				}
				else
				{
					$this->callOSEFirewall();
				}
			}
			else
			{
				return;
			}
		}
	}
	private function signatureUpdate()
	{
		$remoteLogin = new RemoteLogin();
		$remoteLogin->updateSignature();
	}
	private function callOSEFirewall()
	{
		oseFirewall::callLibClass('fwscanner', 'fwscannerbs');
		oseFirewall::callLibClass('fwscanner', 'fwscannerad');
		$oseFirewallScanner = new oseFirewallScannerBasic();
		$oseFirewallScanner->hackScan();
		$oseFirewallScanner = new oseFirewallScannerAdvance();
		$oseFirewallScanner->hackScan();
	}
}
?>
