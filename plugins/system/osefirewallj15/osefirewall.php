<?php
/**
  * @version     2.0 +
  * @package     ProWeb OSE Firewall Plugin
  * @subpackage  Open Source PHP Anti-Hacker for Joomla - com_scanner
  * @author      ProWeb {@link http://www.protect-website.com}
  * @author      Open Source Excellence {@link http://www.opensource-excellence.co.uk}
  * @author      SSRRN {@link http://www.ssrrn.com}
  * @author      Created on 15-Sep-2013
  * @author      Updated on 22-Sep-2013
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  * @Copyright Copyright (C) 2013 ProWeb
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}
jimport('joomla.plugin.plugin');
class plgSystemOsefirewall extends JPlugin
{
	public $params = null; 
	public function plgSystemOsefirewall(& $subject, $config)
	{
		parent :: __construct($subject, $config);
		$plugin= JPluginHelper :: getPlugin('system', 'osefirewall');
		$this->params= new JParameter($plugin->params);
	}
	public function onAfterInitialise()
	{
		$mainframe= JFactory :: getApplication('SITE');
		if($mainframe->isAdmin())
		{
			return; // Dont run in admin
		}
		else
		{
			if ($this->params->get('suiteActivation') == 1)
			{
				$this->callSuite ();
			}
			else
			{
				$this->callOSEFirewall();
			} 
		}
	}
	private function callSuite () {
		$path = $this->params->get('suitePath') ;
		if (file_exists($path))
		{
			require_once($path); 
		}
	}
	private function callOSEFirewall () {
		define('OSEFWDIR', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_firewall'.DS);
		require_once (OSEFWDIR.DS.'protected'.DS.'config'.DS.'define.php');
		require_once (OSE_FWFRAMEWORK.DS.'oseFirewallJoomla.php');
		// Load the OSE Framework ; 
		$oseFirewall = new oseFirewall();
		$oseFirewall -> initSystem ();
		oseFirewall::runYiiApp();
	    $ready = oseFirewall::isDBReady(); 
		if ($ready == true) 
		{
		    oseFirewall::callLibClass('fwscanner','fwscannerbs'); 	
		    $oseFirewallScanner = new oseFirewallScannerBasic (); 
		    $oseFirewallScanner ->hackScan(); 
		}
	}
}
?>
