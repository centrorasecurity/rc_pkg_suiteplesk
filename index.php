<?php
/**
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// Set flag that this is a parent file.
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
	include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';

// Mark afterLoad in the profiler.
JDEBUG ? $_PROFILER->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('site');
if ((isset($_REQUEST['signatureUpdate']) && $_REQUEST['signatureUpdate'] ==1 ) ||
	(isset($_REQUEST['verifyKey']) && $_REQUEST['verifyKey'] ==1 ) ||
	(isset($_REQUEST['updateProfile']) && $_REQUEST['updateProfile'] ==1 ) ||
	(isset($_REQUEST['vsScanning']) && $_REQUEST['vsScanning'] ==1 ) ||
	(isset($_REQUEST['gitbackupv6']) && $_REQUEST['gitbackupv6'] ==1 ) ||
	(isset($_REQUEST['manageweblog']) && $_REQUEST['manageweblog'] ==1 ) ||
	(isset($_REQUEST['fw7stats']) && $_REQUEST['fw7stats'] ==1 ) ||
	(isset($_REQUEST['runBackup']) && $_REQUEST['runBackup'] ==1 ))
{
	// Initialise the application.
	$app->initialise();
}
else
{ 
	$app->redirect(JURI::root()."administrator/index.php");
}
